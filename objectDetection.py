#                                           RESOURCES 
#doesn't take much memory to load in RAM
#https: // github.com/opencv/opencv/tree/master/data/haarcascades
#https://www.youtube.com/watch?v=88HdqNDQsEk

import cv2
import numpy as np 

#define my cascades 
face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
eye_cascade = cv2.CascadeClassifier('haarcascade_eye.xml')

#load in webcam; 0 because of my camera feed; He used 1
cap = cv2.VideoCapture(0)

while True:
    
    ret, img = cap.read()
#convert color to gray
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    faces = face_cascade.detectMultiScale(gray, 1.3, 5)

    for (x,y,w,h) in faces:
#draw a rectangle on the image; starting points (x,y) and ending points (x+w, y+h); blue
        cv2.rectangle(img, (x,y), (x+w, y+h), (255,0,0), 2)
#region of image (roi) is y x not x y; starting points y and x, ending points y+h and x+w
        roi_gray = gray[y:y+h, x:x+w]
        roi_color = img[y:y+h, x: x+w]
#detect against roi_gray
        eyes = eye_cascade.detectMultiScale(roi_gray)
#eyex,eyey,eyewidth,and eyeheight 
        for (ex,ey,ew,eh) in eyes:
#draw on roi_color; starts at ex, ey and stretches out to ex+ew, ey+eh); green 
            cv2.rectangle(roi_color, (ex, ey), (ex+ew, ey+eh), (0, 255,0), 2)
#need to show image
    cv2.imshow('img', img)
    k = cv2.waitKey(30) & 0xff
    if k == 27:
#so we can press the escape key 
        break
cap.release()
cv2.destroyAllWindows()


