# Object Recognition


## Object Recognition
- Recognizes your face with the camera

---
## Contributors
* Detravious Brinkley <d.brinkley97@gmail.com>

---
## Run Code
1. Clone repo
2. Unzip "object_recognition"
3. Open the terminal
4. CD to code
5. Run objectDetection.py

## License & copyright
Contributors

